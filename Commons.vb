﻿Imports System.Text.RegularExpressions
Imports System.IO

Public Class Commons

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Adds a text comma. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="text">         The text. </param>
    ''' <param name="textToAdd">    The text to add. </param>
    ''' <param name="find">         (Optional) True to find. </param>
    '''
    ''' <returns>   . </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Function AddTextComma(text As String, textToAdd As String, Optional find As Boolean = False)
        Dim Trovato = False
        If text = "" Then
            AddTextComma = textToAdd
        Else
            If find Then
                For Each p In Split(text, ",")
                    If p = textToAdd Then
                        Trovato = True
                    End If
                Next
                If Trovato Then
                    AddTextComma = text
                Else
                    AddTextComma = text & "," & textToAdd
                End If
            Else
                AddTextComma = text & "," & textToAdd
            End If
        End If
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets HTML tag. </summary>
    '''
    ''' <remarks>   A Chiarenza, 27/04/2020. </remarks>
    '''
    ''' <param name="TagName">  Name of the tag. </param>
    ''' <param name="HTML">     The HTML. </param>
    '''
    ''' <returns>   The HTML tag. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Function Get_HTMLTag(ByVal TagName As String, ByVal HTML As String) As List(Of String)
        Dim lMatch As New List(Of String) 'Get the results in a List of strings

        If HTML Is Nothing Then
            Return lMatch
        End If

        'RegexOptions.IgnoreCase allows case mismatch e.g. if TagName="title" results can include "title", "Title", "TITLE" etc.
        'RegexOptions.Singleline allows .* to see past CarriageReturn characters 
        Dim Tag As New Regex("(?<=<" & TagName & ">).*(?=<\/" & TagName & ">)", RegexOptions.IgnoreCase Or RegexOptions.Singleline)
        For Each rMatch As Match In Tag.Matches(HTML)
            lMatch.Add(rMatch.Value)
        Next

        Return lMatch
    End Function

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets HTML tag single. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="TagName">  Name of the tag. </param>
    ''' <param name="HTML">     The HTML. </param>
    '''
    ''' <returns>   The HTML tag single. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Function Get_HTMLTag_Single(ByVal TagName As String, ByVal HTML As String) As String
        If HTML Is Nothing Then
            Return ""
        End If

        Dim lMatch As New List(Of String) 'Get the results in a List of strings

        'RegexOptions.IgnoreCase allows case mismatch e.g. if TagName="title" results can include "title", "Title", "TITLE" etc.
        'RegexOptions.Singleline allows .* to see past CarriageReturn characters 
        Dim Tag As New Regex("(?<=<" & TagName & ">).*(?=<\/" & TagName & ">)", RegexOptions.IgnoreCase Or RegexOptions.Singleline)
        For Each rMatch As Match In Tag.Matches(HTML)
            lMatch.Add(rMatch.Value)
        Next

        If lMatch.Count = 0 Then
            Return ""
        Else
            Return lMatch(0)
        End If
    End Function

    Public Shared Sub FileRetention(Path As String, Retention As Integer)
        Dim files As String() = Directory.GetFiles(Path)
        Dim f As FileInfo
        For Each file In files
            f = New FileInfo(file)
            If f.LastWriteTime < DateTime.Now.AddMonths(-Retention) Then
                f.Delete()
            End If
        Next
    End Sub

    Public Shared Function StringToStringList(s As String, Optional sep As String = ";") As List(Of String)
        If s Is Nothing Then
            Return Nothing
        End If
        Dim tmp As List(Of String) = New List(Of String)
        For Each str As String In s.Split(sep)
            tmp.Add(str)
        Next
        Return tmp
    End Function

    Public Shared Function StringListToString(ls As List(Of String), Optional sep As String = ";") As String
        If ls Is Nothing Then
            Return Nothing
        End If
        Dim tmp As String = ""
        For Each s As String In ls
            If tmp = "" Then
                tmp = s
            Else
                tmp = tmp & sep & s
            End If
        Next
        Return tmp
    End Function

End Class
