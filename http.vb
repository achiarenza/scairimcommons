﻿Imports System.Net
Imports System.IO

Public Class Http

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Send message with HTTP. </summary>
    '''
    ''' <remarks>   A Chiarenza, 30/04/2020. </remarks>
    '''
    ''' <param name="MsgBody">              The message body. </param>
    ''' <param name="Url">                  URL of the resource. </param>
    ''' <param name="Headers">              The headers. </param>
    ''' <param name="HttpMethod">           The HTTP method. </param>
    ''' <param name="ResponseType">         Type of the response. </param>
    ''' <param name="ConnnectionTimeout">   The connnection timeout. </param>
    '''
    ''' <returns>   Result of HTTP call. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Function Send(MsgBody As String, Url As String, Headers As Object, HttpMethod As String, ResponseType As String, Optional ConnnectionTimeout As Integer = 60000) As String
        Dim oHttp As HttpWebRequest
        Dim rHttp As HttpWebResponse

        On Error Resume Next
        'If Url.ToLower.Contains("https") Then
        '    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
        'End If

        oHttp = CType(WebRequest.Create(Url), HttpWebRequest)
        oHttp.Timeout = ConnnectionTimeout
        oHttp.Method = HttpMethod
        For x = 0 To UBound(Headers, 1)
            Select Case Headers(x, 0)
                Case "Accept"
                    oHttp.Accept = Headers(x, 1)
                Case "Content-Length"
                    oHttp.ContentLength = CType(Headers(x, 1), Long)
                Case "Content-Type"
                    oHttp.ContentType = Headers(x, 1)
                Case Else
                    oHttp.Headers.Add(Headers(x, 0), Headers(x, 1))
            End Select
        Next

        If Not MsgBody = "" Then
            Using Stream = New StreamWriter(oHttp.GetRequestStream())
                Stream.Write(MsgBody)
            End Using
        End If

        rHttp = CType(oHttp.GetResponse(), HttpWebResponse)

        If Err.Number = 0 Then
            Select Case ResponseType
                Case "text"
                    Using Reader = New StreamReader(rHttp.GetResponseStream())
                        Send = Reader.ReadToEnd()
                    End Using
                Case Else
                    Send = rHttp.StatusCode.ToString()
            End Select
        Else
            Send = "ERROR - " & Err.Number & " - " & Err.Description
        End If

        oHttp.Abort()
        Err.Clear()
    End Function

    Public Shared Function Send(MsgBody As String, Url As String, HttpMethod As String, ResponseType As String, Optional ConnnectionTimeout As Integer = 60000) As String
        Dim oHttp As HttpWebRequest
        Dim rHttp As HttpWebResponse

        On Error Resume Next
        'If Url.ToLower.Contains("https") Then
        '    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
        'End If

        oHttp = CType(WebRequest.Create(Url), HttpWebRequest)
        oHttp.Timeout = ConnnectionTimeout
        oHttp.Method = HttpMethod

        If Not MsgBody = "" Then
            Using Stream = New StreamWriter(oHttp.GetRequestStream())
                Stream.Write(MsgBody)
            End Using
        End If

        rHttp = CType(oHttp.GetResponse(), HttpWebResponse)

        If Err.Number = 0 Then
            Select Case ResponseType
                Case "text"
                    Using Reader = New StreamReader(rHttp.GetResponseStream())
                        Send = Reader.ReadToEnd()
                    End Using
                Case Else
                    Send = rHttp.StatusCode.ToString()
            End Select
        Else
            Send = "ERROR - " & Err.Number & " - " & Err.Description
        End If

        oHttp.Abort()
        Err.Clear()
    End Function

End Class
