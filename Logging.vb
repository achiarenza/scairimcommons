﻿Imports System.IO
Public Class Logging

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Logs to file. </summary>
    '''
    ''' <remarks>   A Chiarenza, 30/04/2020. </remarks>
    '''
    ''' <param name="text">     The text to log. </param>
    ''' <param name="LogPath">  Full pathname of the log file. </param>
    ''' <param name="AppName">  Name of the application. </param>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Sub LogToFile(text As String, LogPath As String, AppName As String)
        Dim lineaTesto
        Dim wr As StreamWriter
        Select Case text
            Case "-"
                lineaTesto = "-----------------------------------------------------------"
            Case ""
                lineaTesto = vbNewLine
            Case Else
                lineaTesto = "[" & Now() & "] - " & text
        End Select
        If File.Exists(LogPath & AppName & "-" & Replace(DateTime.Today.ToShortDateString, "/", "-") & ".txt") Then
            wr = File.AppendText(LogPath & AppName & "-" & Replace(DateTime.Today.ToShortDateString, "/", "-") & ".txt")
        Else
            wr = File.CreateText(LogPath & AppName & "-" & Replace(DateTime.Today.ToShortDateString, "/", "-") & ".txt")
        End If
        wr.WriteLine(lineaTesto)
        wr.Close()
        wr.Dispose()
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Retention"></param>
    ''' <param name="AppName"></param>
    Public Shared Sub LogRetention(Retention As Integer, AppName As String)
        Directory.CreateDirectory(IO.Path.Combine(Directory.GetCurrentDirectory(), "logs_" & AppName))
        Dim path As String = IO.Path.Combine(Directory.GetCurrentDirectory(), "logs_" & AppName)
        Commons.FileRetention(path, Retention)
    End Sub

End Class