﻿Imports System.Data.OleDb
Imports System.Text

Public Class ADODB
    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Data table to excel oledb. </summary>
    '''
    ''' <remarks>   A Chiarenza, 30/04/2020. </remarks>
    '''
    ''' <param name="ExcelFile">        The excel file. </param>
    ''' <param name="datatableExport">  The datatable export. </param>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Sub DataTableToExcelOLEDB(ExcelFile As String, datatableExport As Data.DataTable)

        Dim temp As String
        Dim temp2 As String
        Dim conn As OleDbConnection = New OleDbConnection(GetConnectionString(ExcelFile))

        conn.Open()
        Dim cmd As OleDbCommand = New OleDbCommand
        cmd.Connection = conn

        temp = "CREATE TABLE [table1]("
        For Each column As DataColumn In datatableExport.Columns
            If column.Ordinal = datatableExport.Columns.Count - 1 Then
                temp = temp & Replace(column.ColumnName, " ", "") & " TEXT);"
            Else
                temp = temp & Replace(column.ColumnName, " ", "") & " TEXT, "
            End If
        Next
        cmd.CommandText = temp
        cmd.ExecuteNonQuery()

        temp2 = "INSERT INTO [table1]("
        For Each column As DataColumn In datatableExport.Columns
            If column.Ordinal = datatableExport.Columns.Count - 1 Then
                temp2 = temp2 & Replace(column.ColumnName, " ", "") & ") VALUES("
            Else
                temp2 = temp2 & Replace(column.ColumnName, " ", "") & ","
            End If
        Next

        For Each row As DataRow In datatableExport.Rows
            Try
                temp = temp2
                For Each column As DataColumn In datatableExport.Columns
                    If Not row(column.Ordinal).ToString = "" Then
                        temp = temp & "'" & Replace(row(column.Ordinal).ToString, "'", " ") & "'"
                    Else
                        temp &= "''"
                    End If
                    If Not column.Ordinal = datatableExport.Columns.Count - 1 Then
                        temp &= ","
                    End If
                Next
                temp &= ");"
                cmd.CommandText = temp
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                Dim sb = New StringBuilder()
                For Each item In row.ItemArray
                    sb.Append(Convert.ToString(item))
                    sb.Append(", ")
                Next
                Throw New Exception(ex.Message & ": " & sb.ToString)
            End Try
        Next

        cmd.Dispose()
        conn.Close()
        conn.Dispose()
    End Sub

    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Gets connection string. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="excel">    The excel. </param>
    '''
    ''' <returns>   The connection string. </returns>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Function GetConnectionString(excel As String) As String
        Dim temp As String
        Dim props As Dictionary(Of String, String) = New Dictionary(Of String, String)
        props.Add("Provider", "Microsoft.ACE.OLEDB.12.0")
        props.Add("Extended Properties", "Excel 12.0 XML")
        props.Add("Data Source", excel)

        'props.Add("Provider", "Microsoft.Jet.OLEDB.4.0")
        'props.Add("Extended Properties", "Excel 8.0")
        'props.Add("Data Source", excel)

        Dim sb As StringBuilder = New StringBuilder

        For Each pair In props
            sb.Append(pair.Key)
            sb.Append("=")
            sb.Append(pair.Value)
            sb.Append(";")
        Next

        temp = sb.ToString

        sb.Clear()

        GetConnectionString = temp
    End Function
End Class
