﻿Imports System.Net.Mail

Public Class Mail
    '''////////////////////////////////////////////////////////////////////////////////////////////////////
    ''' <summary>   Sends an email. </summary>
    '''
    ''' <remarks>   A Chiarenza, 28/04/2020. </remarks>
    '''
    ''' <param name="body"> The email body. </param>
    ''' <param name="file"> The file. </param>
    '''////////////////////////////////////////////////////////////////////////////////////////////////////

    Public Shared Sub SendEmail(user As String, password As String, sender As String, receiver As String, SMTP As String, port As Integer, subject As String, body As String, Optional file As String = "")
        Dim Smtp_Server As New SmtpClient
        Dim e_mail As MailMessage
        Smtp_Server.UseDefaultCredentials = False
        Smtp_Server.Credentials = New Net.NetworkCredential(user, password)
        Smtp_Server.Port = port
        Smtp_Server.EnableSsl = True
        Smtp_Server.Host = SMTP

        e_mail = New MailMessage()
        e_mail.From = New MailAddress(sender)
        For Each re In Split(receiver, ";")
            e_mail.To.Add(re)
        Next
        e_mail.Subject = subject '"Scairim Reports: " & Replace(Now.ToShortDateString, "/", "-") & " " & Now.ToShortTimeString
        e_mail.IsBodyHtml = False
        e_mail.Body = body
        If file <> "" Then
            For Each attach In Split(file, ";")
                e_mail.Attachments.Add(New Attachment(attach))
            Next
        End If
        Smtp_Server.Send(e_mail)
        Smtp_Server.Dispose()
        e_mail.Attachments.Dispose()
        e_mail.Dispose()
    End Sub
End Class
